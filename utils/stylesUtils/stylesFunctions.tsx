import { dark, light } from "../../styles/palette";
import { VariantTypes } from "../../types/types";

export const getFontColor = (variant: VariantTypes) => {
  switch (variant) {
    case "light":
      return light.FONT;
    case "dark":
      return dark.FONT;
    default:
      return light.FONT;
  }
};

export const getBackgroundColor = (variant: VariantTypes) => {
  switch (variant) {
    case "light":
      return light.BACKGROUND;
    case "dark":
      return dark.BACKGROUND;
    default:
      return light.BACKGROUND;
  }
};
