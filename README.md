### El passion - recruitment assessment.

Hi, my name is Monika 👋

First, I want to tell you how you can run this project.

1. clone repository https://gitlab.com/magpie-dot/el-passion.git
2. install node modules `npm install`
3. run app `npm run dev`

#### Description

It's a responsive view of a website with posts and podcats in graphic style. I created this according to design from figma ([See the design!](https://www.figma.com/file/eXrgIQffbG5MWp37uIbW4F/newonce_task?node-id=0%3A1))

#### Technologies
- Next.js 
- React
- TypeScript
- Styled Components
- moment.js