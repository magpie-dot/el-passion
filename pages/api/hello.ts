// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

const ARTICLES = [
  {
    id: 1,
    content: {
      title: "Poprzednia dekada nalezala do Drake’a. Jak Kanadyjczyk wplynal muzyke popularna?",
      labels: ["hip-hop"],
    },
    imgSrc: "/images/post-rihanna.png",
    type: "post",
    author: {
      name: "Piotr",
      lastName: "Żelazny",
      publishDate: "Sep 02 2021 11:04:05",
    },
    isBookmarked: false,
  },
  {
    id: 2,
    content: {
      title: "Bodnar: Jakim samochodem jeździ rzecznik",
      subTitle: "Rozmowy: Wojewódzki & Kędzierski",
      labels: [],
    },
    imgSrc: "/images/podcast-beatles.png",
    type: "podcast",
    isBookmarked: false,
  },
  {
    id: 3,
    content: {
      title: "Poprzednia dekada nalezala do Drake’a. Jak Kanadyjczyk wplynal muzyke popularna?",
      labels: ["hip-hop"],
    },
    imgSrc: "/images/post-wheel.png",
    type: "post",
    author: {
      name: "Piotr",
      lastName: "Żelazny",
      publishDate: "Sep 01 2021 13:04:05",
    },
    isBookmarked: false,
  },
  {
    id: 4,
    content: {
      title: "Poprzednia dekada nalezala do Drake’a. Jak Kanadyjczyk wplynal muzyke popularna?",
      labels: ["hip-hop"],
    },
    imgSrc: "/images/post-blue.png",
    type: "post",
    author: {
      name: "Piotr",
      lastName: "Żelazny",
      publishDate: "Aug  31 2021 20:04:05",
    },
    isBookmarked: false,
  },
  {
    id: 5,
    content: {
      title: "Bodnar: Jakim samochodem jeździ rzecznik",
      subTitle: "Rozmowy: Wojewódzki & Kędzierski",
      labels: [],
    },
    imgSrc: "/images/podcast-bolesneporanki.png",
    type: "podcast",
    isBookmarked: false,
  },
  {
    id: 6,
    content: {
      title: "Poprzednia dekada nalezala do Drake’a. Jak Kanadyjczyk wplynal muzyke popularna?",
      labels: ["hip-hop"],
    },
    imgSrc: "/images/post-wheel.png",
    type: "post",
    author: {
      name: "Piotr",
      lastName: "Żelazny",
      publishDate: "Aug 27 2021 18:54:05",
    },
    isBookmarked: false,
  },
  {
    id: 7,
    content: {
      title: "Bodnar: Jakim samochodem jeździ rzecznik",
      subTitle: "Rozmowy: Wojewódzki & Kędzierski",
      labels: [],
    },
    imgSrc: "/images/podcast-bolesneporanki.png",
    type: "podcast",
    isBookmarked: false,
  },
]

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  res.status(200).json({ articles: ARTICLES })
}
