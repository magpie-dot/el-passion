import { useEffect } from 'react';
import type { NextPage } from 'next';
import axios from 'axios';
import useSWRInfinite from 'swr/infinite';
import Post from '../components/Articles/Post/Post';
import Podcast from '../components/Articles/Podcast/Podcast';
import {
  PodcastData,
  PodcastFromApi,
  PostData,
  PostFromApi,
} from '../components/Articles/types';
import Button from '../components/Button';
import { fetchArticles } from '../utils/fetchFunctions';

export const getServerSideProps = () => {
  const data = fetchArticles();
  return {
    props: {
      data,
    },
  };
};

interface HomeProps {
  data: {
    items: (PostFromApi | PodcastFromApi)[];
  };
}

const Home: NextPage<HomeProps> = ({ data: { items } }) => {
  const renderArticle = (article: PostFromApi | PodcastFromApi) => {
    switch (article.type) {
      case 'ARTICLE': {
        const data: PostData = {
          id: article.object.id,
          content: {
            title: article.object.title,
            label: article.object.tag.title,
          },
          imgSrc: article.object.image,
          author: {
            name: article.object.author.name,
            publishDate: article.object.published_at,
          },
          isBookmarked: article.object.bookmarked,
        };
        return <Post postData={data} key={article.object.id} />;
      }
      case 'EPISODE':
        const data: PodcastData = {
          id: article.object.id,
          content: {
            title: article.object.media.title,
            subTitle: article.object.media.subtitle,
          },
          imgSrc: article.object.picture,
        };
        return <Podcast podcastData={data} key={article.object.id} />;
      default:
        return null;
    }
  };
  const getData = () => {
    axios.get(
      'https://api.newonce.me/api/v1/portals/net/feed?page=1&per_page=25'
    );
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      {items.map((article: PostFromApi | PodcastFromApi) =>
        renderArticle(article)
      )}
      <Button content="więcej artykułów" />
    </>
  );
};

export default Home;
