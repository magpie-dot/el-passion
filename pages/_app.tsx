import "../styles/globals.css";
import moment from "moment";
import type { AppProps } from "next/app";
import Layout from "../components/Layout";
import "moment/locale/pl";

function MyApp({ Component, pageProps }: AppProps) {
  moment.locale("pl");
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}
export default MyApp;
