export type VariantTypes = 'dark' | 'light';
export type ArticleTypes = 'post' | 'podcast' | 'ARTICLE' | 'EPISODE';

export enum Articles {
  Post = 'post',
  Podcast = 'podcast',
  ARTICLE = 'ARTICLE',
  EPISODE = 'EPISODE',
}
export interface ParentProps {
  children: JSX.Element | JSX.Element[];
}
