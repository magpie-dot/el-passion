export const palette = {
  ACCENT: "#1300E8",
  INFO: "#788993"
};
export const dark = {
  FONT: "white",
  BACKGROUND: "black",
};

export const light = {
  FONT: "black",
  BACKGROUND: "white",
};
