export const fontFamily = {
  TITLE_FONT: "Helvetica Now Display",
  INFO_FONT: "Helvetica Now Text",
  LABEL_FONT: "Montserrat",
};

export const fontSize = {
  DESKTOP: { TITLE_FONT_SIZE: "24px", SUBTITLE_FONT_SIZE: "14px" },
  MOBILE: {
    POST_TITLE_FONT_SIZE: "16px",
    PODCAST_TITLE_FONT_SIZE: "18px",
    PODCAST_SUBTITLE_FONT_SIZE: "12px",
    INFO_FONT_SIZE: "12px",
  },
  INFO_FONT_SIZE: "12px",
};

export const fontProperties = {
  TITLE_BOLD: 900,
  INFO_BOLD: 700,
  DESKTOP: { TITLE_LINE_HEIGHT: "28px", SUBTITLE_LINE_HEIGHT: "20px" },
  MOBILE: { TITLE_LINE_HEIGHT: "20px", SUBTITLE_LINE_HEIGHT: "16px" },
};
