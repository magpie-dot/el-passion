export const constants = {
  REGULAR_MARGIN: "16px",
  MOBILE_MARGIN: "12px",
  SMALL_MARGIN: "8px",
  SMALLEST_MARGIN: "4px",
  BORDER: "2px solid",
  BOX_SHADOW: "2px 2px 0px #000000",
};

export const sizes = {
  DESKTOP: {
    SCREEN: "828px",
    POST: {
      CONTAINER_HIGHT: "271px",
      IMAGE_HEIGHT: "268px",
      IMAGE_WIDTH: "380px",
    },
    PODCAST: { IMAGE_SIZE: "168px" },
  },
  MOBILE: {
    SCREEN: "375px",
    POST: { IMAGE_HEIGHT: "228px", IMAGE_WIDTH: "339px" },
    PODCAST: { IMAGE_SIZE: "98px" },
  },
  STANDARD_HEIGHT: "48px",
};

export const screenSizes = {
  MOBILE: "750px",
};
