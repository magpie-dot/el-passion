import styled from "styled-components";
import { constants, screenSizes, sizes } from "../../styles/constants";

export const Container = styled.div`
  margin: 0 auto;
  max-width: ${sizes.DESKTOP.SCREEN};
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: ${constants.REGULAR_MARGIN};
  @media (max-width: ${screenSizes.MOBILE}) {
    max-width: ${sizes.MOBILE.SCREEN};
  }
`;
