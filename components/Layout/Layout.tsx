import { ParentProps } from "../../types/types";
import { Container } from "./Layout.css";

const Layout = ({ children }: ParentProps) => <Container>{children}</Container>;

export default Layout;
