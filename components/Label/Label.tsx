import { LabelContainer } from "./Label.css";
import { LabelProps } from "./types";

const Label = ({ text }: LabelProps) => {
  return <LabelContainer>{text}</LabelContainer>;
};

export default Label;
