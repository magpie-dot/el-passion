import styled from 'styled-components';
import { constants } from '../../styles/constants';
import { fontFamily, fontProperties, fontSize } from '../../styles/fonts';
import { palette } from '../../styles/palette';

export const LabelContainer = styled.button`
  max-width: 110px;
  color: white;
  font-family: ${fontFamily.LABEL_FONT};
  font-size: ${fontSize.INFO_FONT_SIZE};
  font-weight: ${fontProperties.TITLE_BOLD};
  text-transform: uppercase;
  background-color: ${palette.ACCENT};
  border: ${constants.BORDER} black;
  box-shadow: ${constants.BOX_SHADOW};
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 16px;
  cursor: pointer;
`;
