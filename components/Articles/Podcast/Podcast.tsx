import { useState } from 'react';
import { PodcastProps } from '../types';
import Container from '../../Container';
import BookmarkIcon from '../../Icons/BookmarkIcon';
import PlayIcon from '../../Icons/PlayIcon';
import ImageContainer from '../../ImageContainer';
import { palette } from '../../../styles/palette';
import { SubTitle, Title } from '../../TextStyles/TextStyles.css';
import { IconButton } from '../../Icons/IconButton.css';
import {
  IconBox,
  IconContainer,
  IconSection,
  InfoContainer,
  MobileContainer,
  TitleSection,
} from './Podcast.css';

const Podcast = ({
  podcastData: {
    content: { title, subTitle },
    imgSrc,
  },
}: PodcastProps) => {
  const [isBookmarked, setIsBookmarked] = useState(false);
  const toggleMarkArticle = () => {
    setIsBookmarked((isBookmarked) => !isBookmarked);
  };

  return (
    <Container variant="dark" articleType="podcast">
      <MobileContainer>
        <ImageContainer variant="dark" src={imgSrc} articleType="podcast" />
        <IconBox>
          <IconContainer>
            <PlayIcon variant="dark" />
          </IconContainer>
          <IconButton onClick={toggleMarkArticle}>
            <IconContainer>
              <BookmarkIcon
                variant="dark"
                fill={isBookmarked ? palette.ACCENT : undefined}
              />
            </IconContainer>
          </IconButton>
        </IconBox>
      </MobileContainer>
      <InfoContainer>
        <TitleSection>
          <SubTitle variant="dark">{subTitle}</SubTitle>
          <Title variant="dark">{title}</Title>
        </TitleSection>
        <IconSection />
      </InfoContainer>
    </Container>
  );
};

export default Podcast;
