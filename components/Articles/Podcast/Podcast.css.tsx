import styled from "styled-components";
import { constants, screenSizes, sizes } from "../../../styles/constants";

export const InfoContainer = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  height: ${sizes.DESKTOP.PODCAST.IMAGE_SIZE};
  @media (max-width: ${screenSizes.MOBILE}) {
    height: auto;
  }
`;

export const TitleSection = styled.section`
  padding: ${constants.REGULAR_MARGIN};
  flex-grow: 1;
  border-bottom: 2px solid white;
  @media (max-width: ${screenSizes.MOBILE}) {
    padding: ${constants.SMALL_MARGIN};
  }
`;

export const IconSection = styled.section`
  height: 48px;
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

export const IconContainer = styled.div`
  width: 48px;
  height: 51px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-left: 2px solid white;
  cursor: pointer;
  @media (max-width: ${screenSizes.MOBILE}) {
    border-right: 2px solid white;
    border-left: 0;
  }
`;

export const IconBox = styled.section`
  position: absolute;
  display: flex;
  bottom: -2px;
  right: 0;
  @media (max-width: ${screenSizes.MOBILE}) {
    position: static;
    padding-left: 2px;
  }
`;

export const MobileContainer = styled.section`
  @media (max-width: ${screenSizes.MOBILE}) {
    display: flex;
    flex-direction: column;
    margin-bottom: -3px;
  }
`;
