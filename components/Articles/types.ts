import { Articles } from '../../types/types';

export interface PostFromApi {
  type: Articles.ARTICLE;
  object: {
    id: number;
    author: {
      name: string;
    };
    bookmarked: boolean;
    image: string;
    published_at: Date;
    tag: {
      id: number;
      title: string;
    };
    title: string;
  };
}

export interface PodcastFromApi {
  type: Articles.EPISODE;
  object: {
    id: number;
    title: string;
    picture: string;
    published_at: Date;
    media: {
      title: string;
      subtitle: string;
    };
  };
}
export interface PostData {
  id: number;
  content: {
    title: string;
    label: string;
  };
  imgSrc: string;
  author: {
    name: string;
    publishDate: Date;
  };
  isBookmarked: boolean;
}

export interface PodcastData {
  id: number;
  content: {
    title: string;
    subTitle: string;
    label?: string;
  };
  imgSrc: string;
  isBookmarked?: boolean;
}

export interface PostProps {
  postData: PostData;
}

export interface PodcastProps {
  podcastData: PodcastData;
}
