import styled from "styled-components";
import { constants, screenSizes } from "../../../styles/constants";
import { fontSize } from "../../../styles/fonts";
import { light } from "../../../styles/palette";

export const InfoContainer = styled.section`
  flex-grow: 1;
  height: 48px;
  padding: ${constants.REGULAR_MARGIN};
  border-bottom: ${constants.BORDER} ${light.FONT};
  @media (max-width: ${screenSizes.MOBILE}) {
    border: none;
    padding: ${constants.MOBILE_MARGIN};
  }
`;

export const InfoSection = styled.section`
  display: flex;
  flex-grow: 1;
  flex-direction: column;
  font-size: ${fontSize.INFO_FONT_SIZE};
  margin-top: -2px;
  @media (max-width: ${screenSizes.MOBILE}) {
    flex-direction: column-reverse;
  }
`;

export const TitleContainer = styled.section`
  padding: 16px 16px 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
  @media (max-width: ${screenSizes.MOBILE}) {
    flex-direction: column-reverse;
    margin-top: -${constants.MOBILE_MARGIN};
    z-index:1;
    padding: 0 ${constants.MOBILE_MARGIN}
  }
`;
