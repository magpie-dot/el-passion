import { useState } from 'react';
import moment from 'moment';
import BookmarkIcon from '../../Icons/BookmarkIcon';
import Container from '../../Container';
import IconContainer from '../../IconContainer';
import ImageContainer from '../../ImageContainer';
import Label from '../../Label';
import { PostProps } from '../types';
import { palette } from '../../../styles/palette';
import { Author, Time, Title } from '../../TextStyles/TextStyles.css';
import { IconButton } from '../../Icons/IconButton.css';
import { InfoContainer, InfoSection, TitleContainer } from './Post.css';

const Post = ({
  postData: {
    imgSrc,
    author: { name, publishDate },
    content: { title, label },
  },
}: PostProps) => {
  const [isBookmarked, setIsBookmarked] = useState(false);

  const toggleMarkArticle = () => {
    setIsBookmarked((isBookmarked) => !isBookmarked);
  };

  const showTime = (date: Date) => {
    const today = new Date();
    const articleDate = new Date(date);
    return moment(articleDate).isSame(today, 'day')
      ? moment(articleDate).fromNow()
      : moment(articleDate).format('ll');
  };

  return (
    <Container variant="light" articleType="post">
      <IconButton onClick={toggleMarkArticle}>
        <IconContainer>
          <BookmarkIcon
            variant="light"
            fill={isBookmarked ? palette.ACCENT : undefined}
          />
        </IconContainer>
      </IconButton>
      <ImageContainer variant="light" src={imgSrc} articleType="post" />
      <InfoSection>
        <InfoContainer>
          <Author> {name} </Author>
          <Time> {showTime(publishDate)} </Time>
        </InfoContainer>
        <TitleContainer>
          <Title variant="light">{title}</Title>
          <Label text={label} />
        </TitleContainer>
      </InfoSection>
    </Container>
  );
};

export default Post;
