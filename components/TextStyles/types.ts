import { VariantTypes } from "../../types/types";

export interface TextStylesProps {
    variant: VariantTypes
}