import styled from "styled-components";
import { constants, screenSizes } from "../../styles/constants";
import { fontProperties, fontSize } from "../../styles/fonts";
import { palette } from "../../styles/palette";
import { getFontColor } from "../../utils/stylesUtils/stylesFunctions";
import { TextStylesProps } from "./types";

export const Title = styled.h3<TextStylesProps>`
  font-weight: ${fontProperties.TITLE_BOLD};
  line-height: ${fontProperties.DESKTOP.TITLE_LINE_HEIGHT};
  font-size: ${fontSize.DESKTOP.TITLE_FONT_SIZE};
  color: ${({ variant }) => getFontColor(variant)};
  cursor: default;
  @media (max-width: ${screenSizes.MOBILE}) {
    font-size: ${fontSize.MOBILE.POST_TITLE_FONT_SIZE};
    line-height: ${fontProperties.MOBILE.TITLE_LINE_HEIGHT};
  }
`;

export const SubTitle = styled.h4<TextStylesProps>`
  line-height: ${fontProperties.DESKTOP.SUBTITLE_LINE_HEIGHT};
  font-size: ${fontSize.DESKTOP.SUBTITLE_FONT_SIZE};
  color: ${({ variant }) => getFontColor(variant)};
  cursor: default;
  @media (max-width: ${screenSizes.MOBILE}) {
    font-size: ${fontSize.MOBILE.PODCAST_SUBTITLE_FONT_SIZE};
    line-height: ${fontProperties.MOBILE.SUBTITLE_LINE_HEIGHT};
  }
`;

export const Author = styled.span`
  font-weight: ${fontProperties.INFO_BOLD};
  color: ${palette.ACCENT};
  cursor: default;
`;

export const Time = styled.data`
  color: ${palette.INFO};
  margin-left: ${constants.SMALL_MARGIN};
  cursor: default;
`;
