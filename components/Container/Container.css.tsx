import styled from 'styled-components';
import { constants, screenSizes } from '../../styles/constants';
import { dark, light } from '../../styles/palette';
import { BorderedContainerProps } from './types';

export const BorderedContainer = styled.article<BorderedContainerProps>`
  border: ${({ articleType }) =>
    articleType === 'post' ? constants.BORDER : undefined};
  border-color: ${({ variant }) =>
    variant === 'light' ? light.FONT : dark.FONT};
  background-color: ${({ variant }) =>
    variant === 'light' ? light.BACKGROUND : dark.BACKGROUND};
  display: flex;
  width: 100%;
  position: relative;
  margin: ${constants.REGULAR_MARGIN} 0;
  @media (max-width: ${screenSizes.MOBILE}) {
    flex-direction: ${({ articleType }) =>
      articleType === 'post' ? 'column' : 'row'};
    margin: ${constants.SMALL_MARGIN} 0;
  }
`;
