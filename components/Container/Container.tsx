import { ContainerProps } from "./types";
import { BorderedContainer } from "./Container.css";

const Container = ({ variant, children, articleType }: ContainerProps) => {
  return (
    <BorderedContainer variant={variant} articleType={articleType}>
      {children}
    </BorderedContainer>
  );
};

export default Container;
