import { ArticleTypes, ParentProps, VariantTypes } from "../../types/types";

export interface ContainerProps extends ParentProps {
  variant: VariantTypes;
  articleType: ArticleTypes;
}

export interface BorderedContainerProps {
  variant: VariantTypes;
  articleType: ArticleTypes;
}
