import { StyledBookmark } from "./Icons.css";
import { IconsProps } from "./types";

const BookmarkIcon = ({variant, fill}:IconsProps) => (
  <StyledBookmark variant={variant} fill={fill}/>
);
export default BookmarkIcon;
