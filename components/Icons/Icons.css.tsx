import styled from "styled-components";
import { getFontColor } from "../../utils/stylesUtils/stylesFunctions";
import Bookmark from "./IconSvgFiles/bookmark.svg";
import Play from "./IconSvgFiles/play.svg"
import { IconsProps } from "./types";

export const StyledBookmark = styled(Bookmark)<IconsProps>`
  width: 10px;
  height: 20px;
  stroke: ${({ variant }) => getFontColor(variant)};
`;

export const StyledPlay = styled(Play)<IconsProps>`
  width: 13px;
  height: 14px;
  stroke: ${({ variant }) => getFontColor(variant)};
`;
