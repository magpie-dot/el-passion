import { VariantTypes } from "../../types/types";

export interface IconsProps {
    variant: string;
    fill?: string;
}