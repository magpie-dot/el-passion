import { StyledPlay } from "./Icons.css";
import { IconsProps } from "./types";


const PlayIcon = ({variant}:IconsProps) => (
  <StyledPlay src="/icons/play.svg" variant={variant}/>
);
export default PlayIcon;
