import styled from "styled-components";

export const IconButton = styled.button`
background-color: inherit; 
border: none; 
cursor: pointer;
&:focus{
    outline: none;
}
`;