import { ParentProps } from "../../types/types";
import { IconBox } from "./IconContainer.css";

const IconContainer = ({ children }: ParentProps) => (
  <IconBox>{children}</IconBox>
);

export default IconContainer;
