import styled from "styled-components";

export const IconBox = styled.div`
  height: 48px;
  width: 48px;
  border: 2px solid black;
  box-shadow: 2px 2px 0px #000000;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  background-color: white;
  z-index: 1;
  right: -2px;
  top: -2px;
  cursor: pointer;
`;
