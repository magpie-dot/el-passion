import { ArticleTypes, VariantTypes } from "../../types/types";

export interface StyledImageContainerProps {
  articleType: ArticleTypes;
  variant: VariantTypes;
}

export interface ImageContainerProps extends StyledImageContainerProps {
  src: string;
}