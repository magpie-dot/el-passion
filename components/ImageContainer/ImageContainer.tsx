import Image from "next/image";
import { ImageContainerProps } from "./types";
import { StyledImageContainer } from "./ImageContainer.css";

const ImageContainer = ({ src, variant, articleType }: ImageContainerProps) => (
  <StyledImageContainer variant={variant} articleType={articleType}>
    <Image src={src} alt="article image" layout="fill" objectFit="cover"/>
  </StyledImageContainer>
);

export default ImageContainer;
