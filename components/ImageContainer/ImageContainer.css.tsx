import styled from "styled-components";
import { constants, screenSizes, sizes } from "../../styles/constants";
import { getFontColor } from "../../utils/stylesUtils/stylesFunctions";
import { StyledImageContainerProps } from "./types";

export const StyledImageContainer = styled.figure<StyledImageContainerProps>`
  min-width: ${({ articleType }) =>
    articleType === "post"
      ? sizes.DESKTOP.POST.IMAGE_WIDTH
      : sizes.DESKTOP.PODCAST.IMAGE_SIZE};
  height: ${({ articleType }) =>
    articleType === "post"
      ? sizes.DESKTOP.POST.IMAGE_HEIGHT
      : sizes.DESKTOP.PODCAST.IMAGE_SIZE};
  border-right: ${constants.BORDER};
  border-color: ${({ variant }) => getFontColor(variant)};
  position: relative;
  align-self: center;
  @media (max-width: ${screenSizes.MOBILE}) {
    min-width: ${({ articleType }) =>
      articleType === "post"
        ? sizes.MOBILE.POST.IMAGE_WIDTH
        : sizes.MOBILE.PODCAST.IMAGE_SIZE};
    height: ${({ articleType }) =>
      articleType === "post"
        ? sizes.MOBILE.POST.IMAGE_HEIGHT
        : sizes.MOBILE.PODCAST.IMAGE_SIZE};
    border-right: ${({ articleType })=> articleType === "post" ? "0" : "2px solid white"};
    border-bottom: ${({ articleType })=> articleType === "post" ? "2px solid black" : "2px solid white"};
  }
`;
