import { ButtonProps } from './types';
import { StyledButton } from './Button.css';

const Button = ({ content, onClick }: ButtonProps) => {
  return <StyledButton onClick={onClick}>{content}</StyledButton>;
};

export default Button;
