import styled from 'styled-components';
import { constants, screenSizes } from '../../styles/constants';

export const StyledButton = styled.button`
  width: 100%;
  height: 48px;
  font-family: Montserrat;
  font-size: 12px;
  font-weight: 900;
  letter-spacing: 0.01em;
  line-height: 18px;
  text-transform: uppercase;
  background-color: white;
  box-shadow: ${constants.BOX_SHADOW};
  border: ${constants.BORDER} black;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 16px 0 14px;
  margin: ${constants.REGULAR_MARGIN} 0;
  outline: none;
  cursor: pointer;
  @media (max-width: ${screenSizes.MOBILE}) {
    margin: ${constants.SMALL_MARGIN} 0;
`;
